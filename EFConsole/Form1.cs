﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EFConsole
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        string oName = nameTextBox.Text;
        string oCompany = companyComboBox.Text;


        private void Contact_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (oName != nameTextBox.Text || oCompany != companyComboBox.Text)
            {
                DialogResult result = MessageBox.Show("Would you like to save your changes",
                    "Save?",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    SaveFormValues();
                }
                else if (result == DialogResult.Cancel)
                {
                    // Stop the closing and return to the form
                    e.Cancel = true;
                }
                else
                {
                    this.Close();
                }
            }
        }


    }
}

