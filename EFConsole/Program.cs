﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities db = new NorthwindEntities();
            //db.Database.Log = Console.WriteLine;
            //foreach (var x in db.Products
            //    .Select(x => new { x.ProductName, x.UnitPrice, x.Category.CategoryName})
            //        )Console.WriteLine(x);

            //foreach (var c in db.Categories)
            //{
            //    Console.WriteLine($"{c.CategoryName} - {c.Products.Count()}");
            //}

            //foreach(var e in db.Employees
            //    .Select (x => new { x.FirstName, x.LastName, ManagerName = x.Manager.LastName})
            //        ) Console.WriteLine(x);

            var qProducts = db.Products // defineerituakse IQueryable Product
                .Where(x => !x.Discontinued); //kehtetuks tunnistatud tooted

            var q2 = qProducts.Select(x => new { x.ProductName, x.UnitPrice }); // defineeritakse

            var q3 = from p in db.Products
                     where !p.Discontinued
                     select new { p.ProductName, p.UnitPrice };

            //foreach (var p in q3) Console.WriteLine(p);  //hakatakse kasutama ja andmebaasi poole pöörduma

            Console.WriteLine(
            db.Products
                .Where(x => x.ProductName == "Lakkalikööri")
                .FirstOrDefault()
                .UnitPrice
                );

            Console.WriteLine(db.Products.Find(76).ProductName);
            // db.Products.Find(76).UnitPrice += 2; // liidab igal käivitamisel unitpricele juurde

            var l = db.Categories.ToList();

            db.Categories.Find(8).CategoryName = "Seatoit";

            db.Products
                .Select(x => new{ x.ProductID, x.ProductName})
                .ToList

            db.SaveChanges(); //salevstab nimemuudatuse ära, saab tagasi muuta..

            Console.WriteLine(string.Join("\n",db.Categories.Select(x => x.CategoryName)));

            //kas on muudatusi
            if (db.ChangeTracker.HasChanges()) db.SaveChanges();
        }
    }
    //partial class Employee
    //{
    //    public string FullName => @"FirstName} {LastName}";
    //}

    private void Form_Closing(object sender, FormClosingEventArgs e)
    {
        if (oName != nameTextBox.Text || oCompany != companyComboBox.Text)
        {
            DialogResult result = MessageBox.Show("Would you like to save your changes",
                "Save?",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Stop);
            if (result == DialogResult.Yes)
            {
                SaveFormValues();
            }
            else if (result == DialogResult.Cancel)
            {
                // Stop the closing and return to the form
                e.Cancel = true;
            }
            else
            {
                this.Close();
            }
        }

    }
