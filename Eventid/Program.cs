﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eventid
{
    class Program
    {
        static void Pidu(Inimene x) => Console.WriteLine($"Täna saab pidu {x.Nimi} saab {x.Vanus} aastaseks ");

        static void Main(string[] args)
        {
            Inimene margit = new Inimene { Nimi = "Margit", Vanus = 29 };

            margit.Juubel += Pidu;
            margit.Juubel += (Inimene x) => Console.WriteLine($"Tuleb minna {x.Nimi}ile kinki ostma");

            Inimene ants = new Inimene { Nimi = "Ants", Vanus = 20 };
            ants.Juubel += Pidu; // pane juubeli juurde action pidu
            ants.Pension += x => Console.WriteLine($"{x.Nimi} mine pensionile"); // siin pensionile juurde action 


            for (int i = 0; i < 35; i++)
            {
                margit.Vanus++;
                ants.Vanus++;
            }

            Console.WriteLine($"{margit.Nimi} on nüüd {margit.Vanus} aastane");
        }
    }

    class Inimene
    {
        public event Action<Inimene> Juubel; //event - miks? 
        public event Action<Inimene> Pension;

        public string Nimi { get; set; }
        int _Vanus;
        public int Vanus
        {
            get => _Vanus;
            set
            {
                _Vanus = value;
                if (_Vanus % 25 == 0)
                {
                    if (Juubel != null) Juubel(this);
                }
                if (_Vanus > 65) Pension?.Invoke(this);
            }


        }

    }
}
