﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolmapaevYlesanneSpordipaevJson
{
    public class Inimene
    {
        public string Nimi { get; set; }
        public string Distants { get; set; }
        public string Aeg { get; set; }

    }

    class Program
    {
        static void Main()
        {
            string loe = System.IO.File.ReadAllText(@"C:\Users\margi\Source\Repos\march-2020-third-week\KolmapaevYlesanneSpordipaevJson\Jooksjate protokoll.json");

            var runners = JsonConvert.DeserializeObject<List<Inimene>>(loe);

            foreach (var x in runners) Console.WriteLine(x.Nimi + " " + x.Aeg);


        }

        static void MainLugemine(string[] args)
        {
            var jooksjad = System.IO.File.ReadAllLines(@"C:\Users\margi\Source\Repos\march-2020-third-week\KolmapaevYlesanneSpordipaevJson\protokoll.txt");

            var jooksjad2 = jooksjad
              .Where(x => x.Trim().Length > 0)
              .Select(x => (x + ",").Split(','))
              .Select(x => new Inimene { Nimi = x[0].Trim(), Distants = x[1].Trim(), Aeg = x[2].Trim() });

            System.IO.File.WriteAllLines(@"C:\Users\margi\Source\Repos\march-2020-third-week\KolmapaevYlesanneSpordipaevJson\Uus protokoll.txt",
                jooksjad2
                .Select(x => $"{x.Nimi}\t{x.Distants}\t{x.Aeg}")

            );

            string json = JsonConvert.SerializeObject(jooksjad2);
            System.IO.File.WriteAllText(@"C:\Users\margi\Source\Repos\march-2020-third-week\KolmapaevYlesanneSpordipaevJson\Jooksjate protokoll.json", json);


        }
    }
}
