﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Harjutus13Uuesti
{
    public partial class Form1 : Form
    {
        NorthwindEntities db = new NorthwindEntities();
       // int categoryID 

        public Form1() // see on konstruktor
        {
            InitializeComponent();

            this.comboBox1.DataSource = db.Categories
            .Select(x => x.CategoryName)
            .ToList()
            .Union(new string[] { "Kõik tooted" }) // lisab string array - kõik tooted
            .ToList()
            ;


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // using (NorthwindEntities db = new NorthwindEntities()) // ainult sellel ajal conncection olemas, siis panna ette using 
            {

                int categoryID = int.TryParse(textBox1.Text, out int i) ? i : 0; //category id ei saa olla 0, algab 1-st

                dataGridView1.DataSource = db.Products
                .Where(x => categoryID == 0 || x.CategoryID == categoryID)
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.UnitsInStock, x.Category.CategoryName })
                .ToList();

                dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Format = "F2"; // siin on võimalik nii värvi kui formatit anda
                dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // joondamine
                dataGridView1.Columns["UnitsInStock"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // joondamine


            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string catName = comboBox1.SelectedItem.ToString();
            textBox1.Text = catName;

            {
                textBox1.Text = db.Categories.Where(x => x.CategoryName == catName).SingleOrDefault()?.CategoryID.ToString();
            }

            Form1_Load(sender e);
        }
    }
}
