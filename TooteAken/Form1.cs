﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TooteAken
{
    public partial class Form1 : Form
    {

        NorthwindEntities db = new NorthwindEntities();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.Products
                //.Select(x => new { x.ProductID, x.ProductName, x.UnitPrice })
                .ToList();

            dataGridView1.Columns["CategoryID"].Visible = false;
            // dataGridView1.
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (db.ChangeTracker.HasChanges()) // haschanges - kas on olemas muudatusi?
            {
                switch(
                MessageBox.Show("Sul on salvestamata muudatusi - kas salvestan?",
                    "Hoiatus!",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Warning
                    ))
                {
                    case DialogResult.Yes: //tuleb salvestada
                        db.SaveChanges();
                        break;
                    case DialogResult.No: // jätame salvestamata
                        break;
                    case DialogResult.Cancel: // ei pane akent kinni
                    e.Cancel = true; // selline asi jätab tegevuse tegemata !! cancel = true
                        break;

                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            db.SaveChanges(); // kui see nupp on lisatud, siis pärast vajutamist enam ei küsi, kas salvesta
        }

       
    }
}
