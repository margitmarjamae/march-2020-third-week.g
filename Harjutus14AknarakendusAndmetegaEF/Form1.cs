﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Harjutus14AknarakendusAndmetegaEF
{
    public partial class Form1 : Form
    {
        NorthwindEntities db = new NorthwindEntities();
        string customerId;

        public Form1()
        {
            InitializeComponent();

            this.comboBox1.DataSource =
                   (new string[] { "Kõik tooted" })
                   .Union(
                        db.Customers
                        .Select(x => x.CompanyName)
                        //.Distinct() // jätab alles ainult unikaaalsed väärtused
                   )
                   .ToList()
                   ;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.Products
                    .Where(x => CustomerID == 0 || x.CustomerID == customerId)
                    .Select(x => new { x.CustomerID, x.CompanyName, x.ContactName, x.Country})
                    .ToList();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string compName = comboBox1.SelectedItem.ToString();

            {
                customerId = db.Customers.Where(x => x.CompanyName == compName).SingleOrDefault()?.CustomerID ?? 0;
            }

            Form1_Load(sender, e);

        }


        
        }


    }
}
