﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spordiaken
{
    class Tulemus
    {
        public string Nimi { get; set; }
        public int Distants { get; set; }
        public double Aeg { get; set; }

        public double Kiirus => Distants / Aeg;
    }
}
