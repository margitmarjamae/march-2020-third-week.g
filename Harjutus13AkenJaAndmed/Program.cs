﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Harjutus13AkenJaAndmed
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }

        static void MainLugemine(string[] args)
        {
            string connectionString = "Data Source=valiitsql.database.windows.net;Initial Catalog=Northwind;Persist Security Info=True;User ID=Student;Password=Pa$$w0rd";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                string commandText = "select productname, unitprice from products";
                using (SqlCommand comm = new SqlCommand(commandText, conn))
                {
                    // või siis nii koos: SqlCommand comm = new SqlCommand("select productname, unitprice from products", conn);

                    var R = comm.ExecuteReader(); // R = sql datareader // tsükkel mis loeb execute readeriga saadu
                    while (R.Read()) // loeb vastused ridade kaupa
                    {
                        // Console.WriteLine($"{R[0]}\t{R[1]}"); // loe esimene, loe teine failist, kaks näidet kuidas.. 
                        Console.WriteLine($"{R["productname"]}\t{R["unitprice"]}");

                    }
                } 
            } 
        }

    }
}
